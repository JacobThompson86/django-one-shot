from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from todos.models import TodoList
from todos.models import TodoItem

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
