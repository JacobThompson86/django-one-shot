from django.contrib import admin

from todos.models import TodoList, TodoItem

# Register your models here.


class ToDoListAdmin(admin.ModelAdmin):
    pass


class TodoItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(TodoList, ToDoListAdmin)
admin.site.register(TodoItem, TodoItemAdmin)
